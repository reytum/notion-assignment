package com.reytum.notion_assignment.views.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.reytum.notion_assignment.data.models.Photo
import com.reytum.notion_assignment.views.viewholders.PhotosViewHolder

class PhotosAdapter : PagingDataAdapter<Photo, PhotosViewHolder>(PHOTOS_DIFF) {
    override fun onBindViewHolder(holder: PhotosViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotosViewHolder {
        return PhotosViewHolder.create(parent)
    }

    companion object {
        private val PHOTOS_DIFF = object : DiffUtil.ItemCallback<Photo>() {
            override fun areItemsTheSame(oldItem: Photo, newItem: Photo): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Photo, newItem: Photo): Boolean =
                oldItem == newItem
        }
    }
}