package com.reytum.notion_assignment.views.callbacks

interface ActivityCommunicator {
    fun onGallerySelected()
    fun onShapesSelected()
}