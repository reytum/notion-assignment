package com.reytum.notion_assignment.views.views

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import com.reytum.notion_assignment.R

class Shape(context: Context, attrs: AttributeSet?) : View(context, attrs) {
    companion object {
        const val CIRCLE = 0
        const val SQUARE = 1
    }

    constructor(context: Context) : this(context, null)

    private val shapePaint: Paint = Paint()
    private val typedArray: TypedArray = context.theme.obtainStyledAttributes(
        attrs,
        R.styleable.Shape, 0, 0
    )
    private var borderColor: Int
    private var borderWidth: Float
    private var type: Int
    private var size: Float
    private var rect: Rect

    init {
        try {
            borderColor =
                typedArray.getInt(
                    R.styleable.Shape_borderColor,
                    R.color.design_default_color_primary
                )
            borderWidth = typedArray.getDimension(R.styleable.Shape_borderWidth, 1.0F)
            type = typedArray.getInt(R.styleable.Shape_type, CIRCLE)
            size = typedArray.getDimension(R.styleable.Shape_size, 10.0F)
            rect = Rect(0, 0, size.toInt(), size.toInt())
        } finally {
            typedArray.recycle()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        shapePaint.style = Paint.Style.STROKE
        shapePaint.strokeWidth = borderWidth
        shapePaint.color = borderColor

        if (type == SQUARE) {
            val left: Float = (measuredWidth - size) / 2
            val top: Float = (measuredHeight - size) / 2
            val right: Float = (measuredWidth + size) / 2
            val bottom: Float = (measuredHeight + size) / 2

            canvas?.drawRect(
                left,
                top,
                right,
                bottom,
                shapePaint
            )
        } else {
            val viewWidthHalf = (measuredWidth / 2).toFloat()
            val viewHeightHalf = (measuredHeight / 2).toFloat()

            canvas?.drawCircle(
                viewWidthHalf,
                viewHeightHalf,
                size / 2,
                shapePaint
            )
        }
    }

    fun setBorderColor(borderColor: Int) {
        this.borderColor = borderColor
        invalidate()
        requestLayout()
    }

    fun setBorderWidth(borderWidth: Float) {
        this.borderWidth = borderWidth
        invalidate()
        requestLayout()
    }

    fun setType(shapeType: Int) {
        this.type = shapeType
        invalidate()
        requestLayout()
    }

    fun setSize(size: Float) {
        this.size = size
        rect = Rect(0, 0, size.toInt(), size.toInt())
        invalidate()
        requestLayout()
    }
}