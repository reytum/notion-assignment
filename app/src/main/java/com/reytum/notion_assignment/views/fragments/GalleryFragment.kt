package com.reytum.notion_assignment.views.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import androidx.recyclerview.widget.GridLayoutManager
import com.reytum.notion_assignment.databinding.FragmentGalleryBinding
import com.reytum.notion_assignment.views.adapters.PhotosAdapter
import com.reytum.notion_assignment.views.viewmodels.PhotosViewModel
import kotlinx.coroutines.launch

class GalleryFragment : Fragment() {
    private var _binding: FragmentGalleryBinding? = null
    private val binding get() = _binding!!
    private val photosViewModel by activityViewModels<PhotosViewModel>()
    private val photosAdapter: PhotosAdapter = PhotosAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentGalleryBinding.inflate(inflater, container, false)
        return binding.root
    }

    @ExperimentalPagingApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        loadImages()
    }

    private fun initViews() {
        binding.photosRecyclerView.layoutManager = GridLayoutManager(context, 2)
        binding.photosRecyclerView.adapter = photosAdapter
    }

    @ExperimentalPagingApi
    private fun loadImages() {
        photosViewModel.fetchImages().observe(viewLifecycleOwner, {
            lifecycleScope.launch {
                photosAdapter.submitData(it)
            }
        })
    }
}