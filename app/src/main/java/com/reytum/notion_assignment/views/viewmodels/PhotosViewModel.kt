package com.reytum.notion_assignment.views.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.reytum.notion_assignment.data.models.Photo
import com.reytum.notion_assignment.data.repositories.PhotosRepository

class PhotosViewModel : ViewModel() {
    private val photosRepository: PhotosRepository = PhotosRepository()

    @ExperimentalPagingApi
    fun fetchImages(): LiveData<PagingData<Photo>> {
        return photosRepository.fetchImages().cachedIn(viewModelScope)
    }
}