package com.reytum.notion_assignment.views.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.reytum.notion_assignment.R
import com.reytum.notion_assignment.data.models.Coordinate
import com.reytum.notion_assignment.data.models.ShapeModel
import com.reytum.notion_assignment.data.models.ShapeModel.Companion.SHAPE_BORDER_WIDTH
import com.reytum.notion_assignment.data.models.ShapeModel.Companion.SHAPE_SIZE
import com.reytum.notion_assignment.data.models.ShapeModel.Companion.getShapeDimension
import com.reytum.notion_assignment.databinding.FragmentShapesBinding
import com.reytum.notion_assignment.utils.Utils
import com.reytum.notion_assignment.views.views.Shape
import java.util.*

class ShapesFragment : Fragment() {
    private var _binding: FragmentShapesBinding? = null
    private val binding get() = _binding!!
    private var screenWidth: Int = 0
    private var screenHeight: Int = 0
    private val shapeStack: MutableList<ShapeModel> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentShapesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val displayDimensions = activity?.let { Utils.getDisplayDimensions(it) }
        screenWidth = displayDimensions?.get(0)!!
        screenHeight = displayDimensions[1]
        setClickListeners()
    }

    private fun setClickListeners() {
        binding.shapeCircle.setOnClickListener {
            addShape(true)
        }

        binding.shapeSquare.setOnClickListener {
            addShape(false)
        }

        binding.buttonUndo.setOnClickListener {
            removeLastShape()
        }

        binding.buttonUndo.setOnLongClickListener {
            binding.shapeContainer.removeAllViews()
            shapeStack.clear()
            Toast.makeText(context, R.string.removed_all_views, Toast.LENGTH_SHORT).show()
            return@setOnLongClickListener true
        }
    }

    private fun addShape(isCircle: Boolean) {
        val usableWidth = binding.shapeContainer.width
        val usableHeight = binding.shapeContainer.height

        checkAndGenerateShape(100, usableWidth, usableHeight, isCircle)
    }

    private fun checkAndGenerateShape(
        retryCount: Int,
        usableWidth: Int,
        usableHeight: Int,
        isCircle: Boolean
    ) {
        val newShapeLocation = getRandomPointOnLayout(
            usableWidth - getShapeDimension(),
            usableHeight - getShapeDimension()
        )

        if (isColliding(newShapeLocation)) {
            if (retryCount == 0) {
                Toast.makeText(
                    context,
                    "Unable to add shape: Insufficient space left, try adding again...",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else {
                checkAndGenerateShape(retryCount - 1, usableWidth, usableHeight, isCircle)
            }
        } else {
            binding.shapeContainer.addView(getShape(isCircle, newShapeLocation))
        }
    }

    private fun isColliding(location: Coordinate): Boolean {
        if (shapeStack.isEmpty()) return false

        for (shapeModel in shapeStack) {
            if (shapeModel.isColliding(location)) {
                return true
            }
        }
        return false
    }

    private fun removeLastShape() {
        removeShapeAtIndex(binding.shapeContainer.childCount - 1)
    }

    private fun removeShapeAtIndex(i: Int) {
        if (i >= 0) {
            shapeStack.removeAt(i)
            binding.shapeContainer.removeViewAt(i)
        } else {
            Toast.makeText(context, R.string.no_shapes_to_remove, Toast.LENGTH_SHORT).show()
        }
    }

    private fun getShape(isCircle: Boolean, newLocation: Coordinate): Shape? {
        val shape = context?.let { Shape(context = it) }

        val params = FrameLayout.LayoutParams(
            getShapeDimension(),
            getShapeDimension()
        )
        params.leftMargin = newLocation.x
        params.topMargin = newLocation.y

        shape?.setSize(SHAPE_SIZE.toFloat())
        shape?.setType(if (isCircle) Shape.CIRCLE else Shape.SQUARE)
        shape?.setBorderColor(R.color.shape_border_color)
        shape?.setBorderWidth(SHAPE_BORDER_WIDTH.toFloat())
        shape?.layoutParams = params
        shape?.post {
            val location = IntArray(2)
            shape.getLocationOnScreen(location)
            shapeStack.add(ShapeModel(isCircle, Coordinate(location[0], location[1])))
        }
        return shape
    }

    private fun getRandomPointOnLayout(layoutWidth: Int, layoutHeight: Int): Coordinate {
        return Coordinate((0..layoutWidth).random(), (0..layoutHeight).random())
    }
}