package com.reytum.notion_assignment.views.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.reytum.notion_assignment.R
import com.reytum.notion_assignment.views.callbacks.ActivityCommunicator
import com.reytum.notion_assignment.views.fragments.GalleryFragment
import com.reytum.notion_assignment.views.fragments.MainFragment
import com.reytum.notion_assignment.views.fragments.ShapesFragment

class MainActivity : AppCompatActivity(), ActivityCommunicator {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(MainFragment())
    }

    private fun replaceFragment(fragment: Fragment) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (!fragment.isAdded && !isFinishing) {
            fragmentTransaction.replace(R.id.container_layout, fragment)
            fragmentTransaction.addToBackStack(fragment.tag)
            fragmentTransaction.commit()
            supportFragmentManager.executePendingTransactions()
        }
    }

    override fun onGallerySelected() {
        replaceFragment(GalleryFragment())
    }

    override fun onShapesSelected() {
        replaceFragment(ShapesFragment())
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            super.onBackPressed()
        }
    }
}