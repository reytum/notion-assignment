package com.reytum.notion_assignment.views.viewholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.reytum.notion_assignment.R
import com.reytum.notion_assignment.data.models.Photo


class PhotosViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    private val photoImageView: ImageView = view.findViewById(R.id.photo_image_view)
    private val noImageTextView: TextView = view.findViewById(R.id.no_image_text)

    fun bind(photo: Photo?) {
        if (photo == null || photo.imageUrl.isNullOrEmpty()) {
            photoImageView.visibility = View.GONE
            noImageTextView.visibility = View.VISIBLE
        } else {
            loadImage(photo.imageUrl)
        }
    }

    private fun loadImage(url: String?) {
        val newUrl =
            "$url&w=336&h=480&dpr=2&fit=crop"
        photoImageView.visibility = View.VISIBLE
        noImageTextView.visibility = View.GONE

        Glide.with(photoImageView).load(newUrl).into(photoImageView)
    }

    companion object {
        fun create(parent: ViewGroup): PhotosViewHolder {
            return PhotosViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.photos_item_layout, parent, false)
            )
        }
    }
}