package com.reytum.notion_assignment

import android.app.Application
import com.reytum.notion_assignment.utils.Utils

class NotionApp : Application() {
    override fun onCreate() {
        super.onCreate()
        Utils.initApp(this)
    }
}