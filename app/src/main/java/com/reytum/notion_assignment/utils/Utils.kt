package com.reytum.notion_assignment.utils

import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Context.WINDOW_SERVICE
import android.content.pm.ApplicationInfo
import android.content.res.Resources
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import kotlin.math.roundToInt

object Utils {
    private lateinit var application: Context

    fun initApp(application: Application) {
        Utils.application = application
    }

    fun getContext(): Context {
        return application
    }

    fun getString(resId: Int): String {
        return application.getString(resId)
    }

    fun printInfoLog(tag: String, message: String) {
        if (isDebugBuild()) {
            Log.i(tag, message)
        }
    }

    fun isDebugBuild(): Boolean {
        return 0 != application.applicationInfo.flags and ApplicationInfo.FLAG_DEBUGGABLE
    }

    fun printErrorLog(tag: String, message: String) {
        if (isDebugBuild()) {
            Log.e(tag, message)
        }
    }

    fun convertDpToPx(dp: Int): Int {
        return (dp * (Resources.getSystem().displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT)).roundToInt()
    }

    fun getDisplayDimensions(activity: Activity): Array<Int> {
        val displayMetrics = DisplayMetrics()
        val windowsManager = application.getSystemService(WINDOW_SERVICE) as WindowManager
        windowsManager.defaultDisplay.getMetrics(displayMetrics)
        return arrayOf(displayMetrics.widthPixels, displayMetrics.heightPixels)
    }
}