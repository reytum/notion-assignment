package com.reytum.notion_assignment.data.database

import androidx.room.*
import com.reytum.notion_assignment.data.database.daos.PhotosDao
import com.reytum.notion_assignment.data.database.daos.RemoteKeysDao
import com.reytum.notion_assignment.data.models.Photo
import com.reytum.notion_assignment.data.models.RemoteKeys
import com.reytum.notion_assignment.utils.Utils

@Database(
    entities = [Photo::class, RemoteKeys::class],
    version = 1,
    exportSchema = false
)

abstract class AppDatabase : RoomDatabase() {
    abstract fun getPhotosDao(): PhotosDao
    abstract fun getRemoteKeysDao(): RemoteKeysDao

    companion object {
        private const val DB_NAME = "notion_db"

        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getInstance(): AppDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase().also { INSTANCE = it }
            }

        private fun buildDatabase() =
            Room.databaseBuilder(Utils.getContext(), AppDatabase::class.java, DB_NAME)
                .build()
    }
}