package com.reytum.notion_assignment.data.repositories

import androidx.paging.*
import com.reytum.notion_assignment.data.database.AppDatabase
import com.reytum.notion_assignment.data.sources.PhotosMediator

class PhotosRepository {
    private val appDatabase = AppDatabase.getInstance()

    @ExperimentalPagingApi
    fun fetchImages() =
        Pager(config = getDefaultPageConfig(), remoteMediator = PhotosMediator(appDatabase)) {
            appDatabase.getPhotosDao().getAll()
        }.liveData

    private fun getDefaultPageConfig(): PagingConfig {
        return PagingConfig(pageSize = 10, enablePlaceholders = true)
    }
}