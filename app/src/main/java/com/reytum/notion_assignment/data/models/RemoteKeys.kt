package com.reytum.notion_assignment.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "remote_keys")
data class RemoteKeys(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val prevKey: Int = 0,
    val nextKey: Int?
)