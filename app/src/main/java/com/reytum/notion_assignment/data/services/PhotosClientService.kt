package com.reytum.notion_assignment.data.services

import com.reytum.notion_assignment.data.models.Photo
import retrofit2.http.GET
import retrofit2.http.Query

interface PhotosClientService {
    @GET("photos")
    suspend fun getPhotos(@Query("page") page: Int): List<Photo>
}