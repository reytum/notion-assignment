package com.reytum.notion_assignment.data.models

import kotlin.math.roundToInt
import kotlin.math.sqrt

data class ShapeModel(val isCircle: Boolean = true, val location: Coordinate) {
    private fun centerDistanceTo(coordinate: Coordinate): Int {
        val targetCenter = getCenter(coordinate)
        val currentCenter = getCenter(location)

        return sqrt(((targetCenter.x - currentCenter.x) * (targetCenter.x - currentCenter.x) + (targetCenter.y - currentCenter.y) * (targetCenter.y - currentCenter.y)).toDouble())
            .roundToInt()
    }

    private fun getCenter(coordinate: Coordinate): Coordinate {
        return Coordinate(
            coordinate.x + getShapeDimension() / 2,
            coordinate.y + getShapeDimension() / 2
        )
    }

    fun isColliding(coordinate: Coordinate): Boolean {
        val distance = centerDistanceTo(coordinate)
        return distance < sqrt(2.0) * SHAPE_SIZE
    }

    companion object {
        const val SHAPE_SIZE = 160
        const val SHAPE_BORDER_WIDTH = 4

        fun getShapeDimension(): Int {
            return SHAPE_SIZE + SHAPE_BORDER_WIDTH
        }
    }
}

data class Coordinate(val x: Int, val y: Int)