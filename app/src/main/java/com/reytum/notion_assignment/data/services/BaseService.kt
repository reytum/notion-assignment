package com.reytum.notion_assignment.data.services

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.reytum.notion_assignment.R
import com.reytum.notion_assignment.utils.Utils
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

open class BaseService {
    companion object {
        const val DEFAULT_BASE_URL: String = "https://api.unsplash.com/"
        private const val AUTH_TOKEN_HEADER: String = "Authorization"
    }

    private var retrofit: Retrofit

    init {
        val gson: Gson = GsonBuilder()
            .setPrettyPrinting()
            .setLenient()
            .create()

        retrofit = Retrofit.Builder()
            .baseUrl(DEFAULT_BASE_URL)
            .client(getHttpClient())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    private fun getHttpClient(): OkHttpClient {
        val okHttpClient = OkHttpClient.Builder()
        if (Utils.isDebugBuild()) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpClient.addInterceptor(interceptor)
        }
        okHttpClient.addInterceptor { chain ->
            val requestBuilder: Request.Builder = chain.request().newBuilder()
            requestBuilder.header("Content-Type", "application/json")
            requestBuilder.header(
                AUTH_TOKEN_HEADER,
                "Client-ID " + Utils.getString(R.string.up_access_key)
            )
            chain.proceed(requestBuilder.build())
        }
        return okHttpClient.build()
    }

    protected fun <T> create(service: Class<T>): T {
        return retrofit.create(service)
    }
}