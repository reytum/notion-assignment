package com.reytum.notion_assignment.data.database.daos

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.reytum.notion_assignment.data.models.PHOTO_TABLE_NAME
import com.reytum.notion_assignment.data.models.Photo

@Dao
interface PhotosDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(photos: List<Photo>)

    @Query("SELECT * FROM $PHOTO_TABLE_NAME")
    fun getAll(): PagingSource<Int, Photo>

    @Query("DELETE FROM photos")
    suspend fun clearAll()
}