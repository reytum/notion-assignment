package com.reytum.notion_assignment.data.models

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

const val PHOTO_TABLE_NAME = "photos"

@Entity(tableName = "photos")
data class Photo(
    @PrimaryKey(autoGenerate = true)
    var key: Int = 0,
    val id: String,
    @Ignore
    val urls: Map<String, String>?,
    var imageUrl: String?
) {
    constructor(id: String, imageUrl: String?) : this(0, id, null, imageUrl)

    fun getUrl(): String? {
        if (!urls.isNullOrEmpty()) {
            return urls["raw"]
        }
        return ""
    }
}
