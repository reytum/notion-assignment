package com.reytum.notion_assignment.data.sources

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.reytum.notion_assignment.data.database.AppDatabase
import com.reytum.notion_assignment.data.models.Photo
import com.reytum.notion_assignment.data.models.RemoteKeys
import com.reytum.notion_assignment.data.services.PhotosService

@ExperimentalPagingApi
class PhotosMediator(private val appDatabase: AppDatabase) : RemoteMediator<Int, Photo>() {

    override suspend fun initialize(): InitializeAction {
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(loadType: LoadType, state: PagingState<Int, Photo>): MediatorResult {
        val loadKey = when (loadType) {
            LoadType.REFRESH -> null
            LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
            LoadType.APPEND -> {
                val remoteKey = appDatabase.withTransaction {
                    appDatabase.getRemoteKeysDao().getRemoteKey()
                }

                if (remoteKey?.nextKey == null) {
                    return MediatorResult.Success(endOfPaginationReached = true)
                }

                remoteKey.nextKey
            }
        } ?: 1

        val response = PhotosService().getPhotos(loadKey)

        appDatabase.withTransaction {
            if (loadType == LoadType.REFRESH) {
                appDatabase.getPhotosDao().clearAll()
                appDatabase.getRemoteKeysDao().clearAll()
            }

            appDatabase.getRemoteKeysDao()
                .insert(RemoteKeys(id = 1, prevKey = loadKey, nextKey = loadKey + 1))
            appDatabase.getPhotosDao().insertAll(response)
        }

        return MediatorResult.Success(endOfPaginationReached = response.isEmpty())
    }
}