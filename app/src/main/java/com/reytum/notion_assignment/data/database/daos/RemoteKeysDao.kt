package com.reytum.notion_assignment.data.database.daos

import androidx.room.*
import com.reytum.notion_assignment.data.models.RemoteKeys

@Dao
interface RemoteKeysDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(remoteKey: RemoteKeys)

    @Query("SELECT * FROM remote_keys WHERE id = 1")
    suspend fun getRemoteKey(): RemoteKeys?

    @Query("DELETE FROM remote_keys")
    suspend fun clearAll()
}