package com.reytum.notion_assignment.data.services

import com.reytum.notion_assignment.data.models.Photo

class PhotosService : BaseService() {
    private val photosClientService: PhotosClientService = create(PhotosClientService::class.java)

    suspend fun getPhotos(page: Int): List<Photo> {
        val data = photosClientService.getPhotos(page)
        data.forEach(action = { it.imageUrl = it.getUrl() })
        return data
    }
}